shaarli (0.13.0+dfsg-4) unstable; urgency=medium

  * Team upload

  [ James Valleroy ]
  * Clean sass cache and qrcode
  * Make test providers static

  [ David Prévot ]
  * Group nophpunit11 for test failing with PHPUnit 11 (Closes: #1039855)
  * Rename PluginQrcodeTest as PluginReadItLaterTest (PHPUnit 11 fix)

 -- David Prévot <taffit@debian.org>  Wed, 15 Jan 2025 10:45:56 +0100

shaarli (0.13.0+dfsg-3) unstable; urgency=medium

  * Team upload
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Thu, 07 Mar 2024 17:48:27 +0100

shaarli (0.13.0+dfsg-2) unstable; urgency=medium

  * postinst: Enable apache rewrite module (Closes: #1032919)
  * tests: Check install page messages
  * Remove compiling message for blhc
  * links: Link forkawesome fonts into assets folder
  * Update copyright year

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 09 Feb 2024 16:16:03 -0500

shaarli (0.13.0+dfsg-1) unstable; urgency=medium

  * New upstream version 0.13.0+dfsg (Closes: #1043259)
  * Refresh patches
  * Add build-deps for sphinx document generation
  * Apply upstream patch for PHP 8.x intl incompatibility
  * Add dependency on php-klogger
  * Remove obsolete patches
  * d/clean: Specify css directory
  * Remove .doctrees folder after building docs
  * Re-number patches
  * Drop fonts that are packaged separately
  * Depend on fonts-fork-awesome (Closes: #1040575)
  * Links to packaged Roboto fonts
  * Link to packaged sphinx JS files
  * Set standards version to 4.6.2
  * Update d/copyright
  * Minify qrcode JS during build (Closes: #1032920)

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 15 Dec 2023 11:46:30 -0500

shaarli (0.12.1+dfsg-8) unstable; urgency=medium

  * Allow AuthConfig in .htaccess
  * Create link for cache folder (Closes: #1029921)

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 29 Jan 2023 07:36:49 -0500

shaarli (0.12.1+dfsg-7) unstable; urgency=medium

  * Add copyright info for pure-extras
  * webpack: css-loader: Disable url() resolving
  * Add symlinks to font files (Closes: #1027401)

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 14 Jan 2023 20:42:54 -0500

shaarli (0.12.1+dfsg-6) unstable; urgency=medium

  * Cherry-pick date view fix from upstream

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 31 Oct 2022 21:59:59 -0400

shaarli (0.12.1+dfsg-5) unstable; urgency=medium

  * Install docs
  * Limit output of apache-test

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 16 Oct 2022 09:07:31 -0400

shaarli (0.12.1+dfsg-4) unstable; urgency=medium

  * d/tests: Add apache test
  * Update debian/* copyright year

 -- James Valleroy <jvalleroy@mailbox.org>  Wed, 05 Oct 2022 06:50:31 -0400

shaarli (0.12.1+dfsg-3) unstable; urgency=medium

  * mkdocs: Change pages to nav
  * Drop patch 0010-webpack-Use-uglifyjs-instead-of-terser (Closes: #1011447)
  * webpack: Resolve from system install paths
  * Set standards version to 4.6.1

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 27 May 2022 09:12:44 -0400

shaarli (0.12.1+dfsg-2) unstable; urgency=medium

  * Skip test that depends on DNS setup

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 15 Jan 2022 10:07:12 -0500

shaarli (0.12.1+dfsg-1) unstable; urgency=medium

  * d/README.source: Checkout upstream before merge
  * New upstream version 0.12.1+dfsg
  * Update patches for v0.12.1
  * Build-Depend on php-malkusch-lock
  * Build-Depend on php-parsedown-extra
  * Skip more tests that require Internet access
  * Add override for php-klogger
  * Skip additional tests due to root user
  * Skip tests that depend on working directory
  * Build-Depend on node-he
  * Don't ignore errors when running webpack
  * Move history into /var/lib/shaarli
  * Specify locale when running tests
  * Skip testReadNotReadable test

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 07 Jan 2022 12:08:25 -0500

shaarli (0.12.0+dfsg-4) unstable; urgency=medium

  * Fix doc build using upstream patch (Closes: #997328)
  * d/control: Set standards version to 4.6.0

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 06 Nov 2021 12:16:57 -0400

shaarli (0.12.0+dfsg-3) unstable; urgency=low

  * Compile scss and add build dependencies
  * Use purecss from sass-stylesheets-purecss
  * Use vendored pure-extras
  * Use installed fork-awesome fonts (Closes: #980134)

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 03 Sep 2021 19:21:13 -0400

shaarli (0.12.0+dfsg-2) unstable; urgency=low

  * Upload to unstable
  * Fix tests during package build
  * Run webpack during build but ignore errors
  * webpack: Use uglifyjs instead of terser
  * testHumanBytes: Skip 2 GiB test on 32-bit systems
  * Set standards version to 4.5.1

 -- James Valleroy <jvalleroy@mailbox.org>  Wed, 13 Jan 2021 20:48:01 -0500

shaarli (0.12.0+dfsg-1) experimental; urgency=medium

  * Import new upstream release v0.12.0
  * d/control: Clarify short description
  * d/README.source: Update instructions for new upstream release
  * d/patches: Update patches to apply to 0.12.0 upstream
  * Remove obsolete lintian override
  * d/install: Install .htaccess
  * Remove LimitExcept from top level .htaccess
  * Rename patches to have consecutive order
  * d/control: Add php-ldap dependency
  * Update skipped tests under root user

 -- James Valleroy <jvalleroy@mailbox.org>  Thu, 12 Nov 2020 09:13:48 -0500

shaarli (0.11.1+dfsg-1) experimental; urgency=medium

  * Initial release (Closes: #864559).

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 11 Oct 2020 09:23:56 -0400
