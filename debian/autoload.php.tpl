<?php

require_once '/usr/share/php/Gettext/autoloader.php';
require_once '/usr/share/php/Katzgrau/KLogger/autoload.php';
require_once '/usr/share/php/Malkusch/Lock/autoload.php';
require_once '/usr/share/php/Parsedown/autoload.php';
require_once '/usr/share/php/ParsedownExtra/autoload.php';
require_once '/usr/share/php/Pubsubhubbub/Publisher/autoload.php';
require_once '/usr/share/php/Shaarli/NetscapeBookmarkParser/autoload.php';
require_once '/usr/share/php/Slim/autoload.php';
require_once '/usr/share/php/WebThumbnailer/autoload.php';

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require ___BASEDIR___$classes[$cn];
        }
    },
    true,
    false
);
// @codeCoverageIgnoreEnd
